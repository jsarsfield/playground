from MultiNEAT import EvaluateGenomeList_Parallel

def get_fitnesses(genome_list, task):
    return EvaluateGenomeList_Parallel(genome_list, task.evaluate, cores=8, display=False)