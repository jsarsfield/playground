import environment
import MultiNEAT as NEAT
from statistics import mean
from multiprocessing import Process, current_process

def get_genome(filename=None):
    if filename is None:
        return NEAT.Genome(0,
                            environment.env.observation_space.shape[0],#substrate.GetMinCPPNInputs(),
                            0,
                            environment.env.action_space.shape[0],#substrate.GetMinCPPNOutputs(),
                            False,
                            NEAT.ActivationFunction.TANH,
                            NEAT.ActivationFunction.TANH,
                            0,
                            params)
    else:
        return NEAT.Genome(filename)

def evaluate(genome, runs=5, visualise=False):
    rewards = []
    for run in range(runs):
        totalReward = 0
        completedTimeStep = 0
        observation = environment.env.reset()
        net = NEAT.NeuralNetwork()
        genome.BuildPhenotype(net)#, substrate, params)
        for i in range(environment.env.spec.timestep_limit+1):
            if visualise:
                environment.env.render()
            #net.Flush()
            net.Input(observation.tolist())
            net.Activate()
            outputs = list(net.Output())
            action = outputs.index(max(outputs))
            '''
            action = int(math.ceil((environment.env.action_space.n/2)*(outputs[0]+1))-1)
            action = 0 if action == -1 else action
            if action != 0 and action != 1:
                a=1
            '''
            observation, reward, done, info = environment.env.step(outputs)
            totalReward+=reward
            if done or i == environment.env.spec.timestep_limit:
                completedTimeStep = i
                rewards.append(totalReward)
                break
    reward = mean(rewards)
    print("Reward: " + str(reward))
    return reward #{'fitness':self.totalReward}

if current_process().name == 'MainProcess':
    params = NEAT.Parameters()
    params.PopulationSize = 100;

    params.DynamicCompatibility = True;
    params.CompatTreshold = 3.0;
    params.YoungAgeTreshold = 15;
    params.SpeciesMaxStagnation = 100;
    params.OldAgeTreshold = 35;
    params.MinSpecies = 8;
    params.MaxSpecies = 15;
    params.RouletteWheelSelection = False;

    params.MutateRemLinkProb = 0.02;
    params.RecurrentProb = 0.9;
    params.OverallMutationRate = 0.15;
    params.MutateAddLinkProb = 0.3;
    params.MutateAddNeuronProb = 0.03;
    params.MutateWeightsProb = 0.80;
    params.MaxWeight = 5.0;
    params.WeightMutationMaxPower = 0.2;
    params.WeightReplacementMaxPower = 1.0;

    params.MutateActivationAProb = 0.0;
    params.ActivationAMutationMaxPower = 0.5;
    params.MinActivationA = 0.05;
    params.MaxActivationA = 6.0;

    params.MutateNeuronActivationTypeProb = 0.03;

    params.ActivationFunction_SignedSigmoid_Prob = 0.0;
    params.ActivationFunction_UnsignedSigmoid_Prob = 0.0;
    params.ActivationFunction_Tanh_Prob = 1.0;
    params.ActivationFunction_TanhCubic_Prob = 0.0;
    params.ActivationFunction_SignedStep_Prob = 0.0;
    params.ActivationFunction_UnsignedStep_Prob = 0.0;
    params.ActivationFunction_SignedGauss_Prob = 0.0;
    params.ActivationFunction_UnsignedGauss_Prob = 0.0;
    params.ActivationFunction_Abs_Prob = 0.0;
    params.ActivationFunction_SignedSine_Prob = 0.0;
    params.ActivationFunction_UnsignedSine_Prob = 0.0;
    params.ActivationFunction_Linear_Prob = 0.0;

    params.AllowClones = True
    params.EliteFraction = 0.1
    params.SurvivalRate = 0.2

    params.DivisionThreshold = 0.5;
    params.VarianceThreshold = 0.03;
    params.BandThreshold = 0.3;
    params.InitialDepth = 2;
    params.MaxDepth = 4;
    params.IterationLevel = 1;
    params.Leo = False;
    params.GeometrySeed = False;
    params.LeoSeed = False;
    params.LeoThreshold = 0.3;
    params.CPPN_Bias = -1.0;
    params.Qtree_X = 0.0;
    params.Qtree_Y = 0.0;
    params.Width = 2.;
    params.Height = 2.;
    params.Elitism = 0.3;