#!/usr/bin/python3
import os
import sys
import time
import random as rnd
import subprocess as comm
import numpy as np
import pickle as pickle
import MultiNEAT as NEAT
from MultiNEAT import GetGenomeList, ZipFitness
import environment
import math, time
from time import gmtime, strftime
from statistics import mean
import MainTask
import msvcrt

import importlib

def getbest(run):
    global BestFitness
    global BestGenome

    pop = NEAT.Population(BestGenome, task.params, True, 1.0, rnd.randint(0, 1000))
    for generation in range(100000):
        #Evaluate genomes
        genome_list = NEAT.GetGenomeList(pop)

        fitnesses = MainTask.get_fitnesses(genome_list, task)
        [genome.SetFitness(fitness) for genome, fitness in zip(genome_list, fitnesses)]
        
        print('Gen: %d Best: %3.5f' % (generation, max(fitnesses)))

        # Print best fitness
        #print("---------------------------")
        #print("Generation: ", generation)
        #print("max ", max([x.GetLeader().GetFitness() for x in pop.Species]))
        
        
        maxFitness = max(fitnesses)
        if maxFitness > BestFitness:
                BestFitness = maxFitness
                BestGenome = pop.GetBestGenome()
        if msvcrt.kbhit():
            c = ord(msvcrt.getch())
            print("Key pressed: %s %s" % (chr(c),c))
            time.sleep(1)
            # Save best genomes and finish
            if c == 27: # ESC
                generations = generation
                break
            # Visualise pop best
            elif c == 13: # Enter
                try: 
                    task.evaluate(BestGenome,runs=1000, visualise=True)
                except:
                    environment.env.viewer = None
                    pass
            # Save best genome and continue
            elif c == 115: # s
                save_best_genomes()
            # Visualize best network's Genome
            elif c == 118: # v
                draw_network(BestGenome)
        if maxFitness > (environment.env.spec.reward_threshold+10):
            generations = generation
            break
            '''
            bestFitnesses = []
            for run in range(100):
                bestFitnesses.append(evaluate(BestGenome, visualise=False))
            print("****Genome Mean: "+str(mean(bestFitnesses)))
            if mean(bestFitnesses) > environment.env.spec.reward_threshold:
                generations = generation
                break
            '''
        # Epoch
        generations = generation
        pop.Epoch()
    save_best_genomes()
    return generations

def save_best_genomes():
    BestGenome.Save("BestGenome"+environment.env.spec.id+strftime("_%Y_%m_%d_%H-%M-%S", gmtime()))

def draw_network(genome):
    cv2 = importlib.import_module("cv2")
    viz = importlib.import_module("MultiNEAT.viz")
    net = NEAT.NeuralNetwork()
    genome.BuildPhenotype(net)
    img = np.zeros((500, 500, 3), dtype=np.uint8)
    img += 10
    viz.DrawPhenotype(img, (0, 0, 500, 500), net )
    while(1):
        cv2.imshow("CPPN", img)
        k = cv2.waitKey(33)
        if k==27:    # Esc key to stop
            cv2.destroyAllWindows() 
            break

if __name__ == '__main__':
    task = importlib.import_module('Tasks.'+environment.env.spec.id.replace("-","_")+'Task')
    rng = NEAT.RNG()
    rng.TimeSeed()
    '''
    substrate = NEAT.Substrate([(-1., -1., 0.0), (1., -1., 0.0)],
                               [],
                               [(0.0, 1., 0.0)]) #, (1.0,1.0,0.0)])

    substrate.m_allow_input_hidden_links = False;
    substrate.m_allow_input_output_links = False;
    substrate.m_allow_hidden_hidden_links = False;
    substrate.m_allow_hidden_output_links = False;
    substrate.m_allow_output_hidden_links = False;
    substrate.m_allow_output_output_links = False;
    substrate.m_allow_looped_hidden_links = False;
    substrate.m_allow_looped_output_links = False;

    substrate.m_allow_input_hidden_links = True;
    substrate.m_allow_input_output_links = False;
    substrate.m_allow_hidden_output_links = True;
    substrate.m_allow_hidden_hidden_links = False;

    substrate.m_hidden_nodes_activation = NEAT.ActivationFunction.TANH;
    substrate.m_output_nodes_activation = NEAT.ActivationFunction.TANH;

    substrate.m_with_distance = False;

    substrate.m_max_weight_and_bias = 5.0;
    '''
    gens = []
    BestGenome = task.get_genome("BestGenomeBipedalWalker-v2_2016_10_14_20-32-15")#"BestGenome"+environment.env.spec.id+"_2016_10_11_20-11-42")
    BestFitness = -math.inf
    for run in range(1):
        gen = getbest(run)
        gens += [gen]
        print('Run:', run, 'Generations to solve XOR:', gen)
    avg_gens = sum(gens) / len(gens)
    environment.env.monitor.start('/tmp/'+environment.env.spec.id,force=True)
    print("start")
    task.evaluate(BestGenome,runs=100, visualise=False)
    environment.env.monitor.close()
    print('All:', gens)
    print('Average:', avg_gens)

