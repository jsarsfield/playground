import gym
from random import randint
import math
import environment
from peas.methods.neat import NEATPopulation, NEATGenotype
from peas.networks.rnn import NeuralNetwork
from CartPoleTask import CartPoleTask
import operator

if __name__ == "__main__":
    bTest = True
    # Create a factory for genotypes (i.e. a function that returns a new 
    # instance each time it is called)
    genotype = lambda: NEATGenotype(inputs=environment.env.observation_space.shape[0], outputs=environment.env.action_space.n, #shape[0],
                                    weight_range=(-5., 5.), 
                                    types=['tanh'])

    # Create a population
    pop = NEATPopulation(genotype, popsize=150, stop_when_solved=True, tournament_selection_k=5, max_cores=0)#math.inf)
    
    # Create a task
    dpnv = CartPoleTask()

    # Run the evolution, tell it to use the task as an evaluator
    pop.epoch(generations=3000, evaluator=dpnv, solution=dpnv)
    if bTest:
        print("Begin test")
        elite = max(pop.champions, key=lambda ind:ind.stats['fitness'])
        network = NeuralNetwork(elite)
        org = CartPoleTask()
        environment.env.monitor.start('/tmp/'+environment.env.spec.id,force=True)
        for i_episode in range(environment.env.spec.trials):
            org.evaluate(network,visualise=True)
        environment.env.monitor.close()
        print("End test")