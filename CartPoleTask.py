import environment
from peas.networks.rnn import NeuralNetwork
import math

class CartPoleTask():
    def __init__(self):
        pass

    def evaluate(self, network, verbose=False, visualise=False):
        self.totalReward = 0
        self.completedTimeStep = 0
        observation = environment.env.reset()
        if not isinstance(network, NeuralNetwork):
            network = NeuralNetwork(network)
        for i in range(environment.env.spec.timestep_limit+1):
            if visualise:
                environment.env.render()
            action = int(math.ceil((environment.env.action_space.n/2)*(network.feed( observation )[-1]+1))-1)
            action = 0 if action is -1 else action
            if action is not 0 and action is not 1:
                a=1
            observation, reward, done, info = environment.env.step(action)
            self.totalReward+=reward
            if done or i == environment.env.spec.timestep_limit:
                print("Reward: " + str(self.totalReward))
                self.completedTimeStep = i
                return {'fitness':self.totalReward}

    def solve(self, network):
        if 'MountainCar' in environment.env.spec.id:
            return True if self.totalReward > environment.env.spec.reward_threshold else False
        elif 'CartPole' in environment.env.spec.id:
            return True if self.totalReward > environment.env.spec.reward_threshold else False